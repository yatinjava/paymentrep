package com.stock.payment.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stock.payment.dto.CardDetailAndPrice;
import com.stock.payment.exception.PaymentException;
import com.stock.payment.service.PaymentService;

@RestController
@RequestMapping("payment")
public class PaymentController {
	
	@Autowired
	private PaymentService paymentService;

	@PostMapping(value="/do",consumes = MediaType.APPLICATION_JSON)
	public String makePayment(@RequestBody CardDetailAndPrice cardDetailsAndPrice) throws PaymentException {
		
		String status = paymentService.pay(cardDetailsAndPrice);
		
		return status;
		
		
	}
	
}
