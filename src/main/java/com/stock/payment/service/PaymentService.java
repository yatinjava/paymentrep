package com.stock.payment.service;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.stock.payment.dto.CardDetailAndPrice;
import com.stock.payment.exception.PaymentException;

@Service
public class PaymentService {

	public static Logger logger = LoggerFactory.getLogger(PaymentService.class);

	public String pay(CardDetailAndPrice cardDetails ) throws PaymentException{

		logger.info("making payments for card number "+ cardDetails.getCardNumber());
		String paymentStatus = null;
		
		if((cardDetails != null && null != cardDetails.getCardNumber()
				&& null != cardDetails.getCvv()
				&& null != cardDetails.getAmount())
				&& 1 == cardDetails.getAmount().compareTo(BigDecimal.ZERO)) {
			String cardNumber = cardDetails.getCardNumber();
			String cvv = cardDetails.getCvv();
			if(StringUtils.isNotEmpty(cardNumber)&& StringUtils.isNotEmpty(cvv)&& cardNumber.length() == 16 && cvv.length() ==3) {
				paymentStatus = "Payment Success";	 
			}
		}
		else {
			logger.info("payment failed for card number "+cardDetails.getCardNumber());
			logger.error("payment failed for card number "+cardDetails.getCardNumber());
			throw new PaymentException("Exception occured in payment. Check card details");
		}
		
		return paymentStatus;
	}

}
